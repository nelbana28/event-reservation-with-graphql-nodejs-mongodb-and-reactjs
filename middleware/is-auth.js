const jwt = require('jsonwebtoken');
let isAuth = (req, res, next) => {
    const authHeader = req.get('Authorization');
   // console.log(req.headers)
    if (!authHeader) {
        req.isAuth = false;
        return next();
    }
    const token = authHeader.split(' ')[1];
    console.log(token)
    if (!token || token === '') {
        req.isAuth = false;
        return next();
    }
    let decodedToken;
    try {
        decodedToken = jwt.verify(token, 'supersecret');
    } catch (err) {
       // console.log(err)
        req.isAuth = false;
        return next();
    }
   // console.log(decodedToken)
    if (!decodedToken) {
        req.isAuth = false;
        return next();
    }
     req.isAuth = true;
    // console.log(req.isAuth)
    req.userId = decodedToken.userId;
    console.log(req.userId)
    next();
}
module.exports={isAuth}