const Event = require('../../models/event');
const Booking = require('../../models/booking');
const { singleEvent, user } = require('./merge');


const transformBooking = booking => {
    return { ...booking._doc, user: user.bind(this, booking._doc.user), event: singleEvent.bind(this, booking._doc.event) }
}
const transformEvent = event => {
    return {
        ...event._doc, date: new Date(event._doc.date).toISOString(),
        creator: user.bind(this, event.creator)
    }
}
module.exports = {
    bookings: async (req) => {
        try {
            if (!req.isAuth) {
                throw new Error('unauthenticated')
            }
            const bookings = await Booking.find()
            return bookings.map(booking => {
                return transformBooking(booking)
            })
        } catch (err) {
            throw err
        }
    },
    bookEvent: async (args,req) => {
        if (!req.isAuth) {
            throw new Error('unauthenticated')
        }
        const fetchedEvent = await Event.findOne({ _id: args.eventId })
        const booking = new Booking({
            user: req.userId,
            event: fetchedEvent
        })
        const result = await booking.save()
        return transformBooking(result)
    },
    cancelBooking: async (args,req) => {
        try {
            if (!req.isAuth) {
                throw new Error('unauthenticated')
            }
            const booking = await Booking.findById(args.bookingId).populate('event')
            const event = transformEvent(booking.event)
            await Booking.deleteOne({ _id: args.bookingId })
            return event
        } catch (err) {
            console.log(err)
            throw err
        }
    }
}
