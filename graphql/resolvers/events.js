const Event = require('../../models/event');
const { user } = require('./merge');
const User = require('../../models/user');

const transformEvent = event => {
    return {
        ...event._doc, date: new Date(event._doc.date).toISOString(),
        creator: user.bind(this, event.creator)
    }
}
module.exports = {
    events: (req) => {
        console.log(req)
        if (!req.isAuth) {
            throw new Error('unauthenticated')
        }
        return Event.find()
            .then(events => {
                return events.map(event => {
                    return transformEvent(event)
                })
            })
            .catch(err => {
                throw err
            })
    },

    createEvent: (args, req) => {
        console.log(req.isAuth)
        console.log(req.userId)
        if (!req.isAuth) {
            throw new Error('unauthenticated')
        }
        const event = new Event({
            title: args.eventInput.title,
            description: args.eventInput.description,
            price: +args.eventInput.price,
            date: args.eventInput.date,
            creator: req.userID
        })
        return event
            .save()
            .then(result => {
                createdEvent = transformEvent(result)
                return User.findById(req.userId)
            })
            .then(user => {
                if (!user) {
                    throw new Error('user doesnt exist')
                }
                user.createdEvents.push(event)
                return user.save()
            })
            .then(result => {
                return createdEvent
            })
            .catch(err => {
                throw err
            })
    }
}
