const bcrypt = require('bcryptjs');
const User = require('../../models/user');
const jwt = require('jsonwebtoken');


module.exports = {
    createUser: args => {
        return User.findOne({ email: args.userInput.email }).then(user => {
            if (user) {
                throw new Error('user already exists')
            }
            return bcrypt.hash(args.userInput.password, 12)
        })
            .then(hashedPassword => {
                const user = new User({
                    email: args.userInput.email,
                    password: hashedPassword
                })
                return user.save();
            })
            .then(result => {
                return { ...result._doc, password: null }
            })
            .catch(err => {
                throw err;
            })
    },
    login: async ({ email, password }) => {
        const user = await User.findOne({ email })
        if (!user) {
            throw new Error("user doesnt exist");
        }
        const isEqual = await bcrypt.compare(password, user.password)
        if (!isEqual) {
            throw new Error("password is incorrect")
        }
        const token = jwt.sign({ userId: user.id, email: user.email }, 'supersecret', {
            expiresIn: '1h'
        });
        return { userId: user._id, token, tokenExpiration: 1 }
    }
}
