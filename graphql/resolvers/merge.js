const Event = require('../../models/event');
const User = require('../../models/user');

const user = userId => {
    return User.findById(userId).then(user => { return { ...user._doc, createdEvents: events.bind(this, user._doc.createdEvents) } }).catch(err => { throw err })
}
const transformEvent = event => {
    return {
        ...event._doc, date: new Date(event._doc.date).toISOString(),
        creator: user.bind(this, event.creator)
    }
}
const events = eventIds => {
    return Event.find({ _id: { $in: eventIds } }).then(events => {
        return events.map(event => {
            return transformEvent(event)
        })
    }).catch(err => {
        throw err
    })
}
const singleEvent = async eventId => {
    try {
        const event = await Event.findById(eventId)
        return transformEvent(event)
    } catch (err) {
        throw err
    }
}
exports.events = events;
exports.singleEvent = singleEvent;
exports.user = user;
