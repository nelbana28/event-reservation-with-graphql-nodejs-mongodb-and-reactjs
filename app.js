const express = require('express');
const app = express();
const graphqlHttp = require('express-graphql');
const bodyParser = require('body-parser');
app.use(bodyParser.json());
const mongoose = require('mongoose');
const graphqlSchema = require('./graphql/schema/index');
const graphqlResolvers = require('./graphql/resolvers/index');
const { isAuth } = require('./middleware/is-auth')
app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'POST,GET,OPTIONS');
    res.setHeader('Access-Control-Allow-Headers', 'Content-Type,Authorization');
    if (req.method === 'OPTIONS') {
        return res.sendStatus(200);
    }
    next();
});
app.use(isAuth);
app.use('/graphql', graphqlHttp({
    schema: graphqlSchema,
    rootValue: graphqlResolvers,
    graphiql: true
}))
mongoose.connect(`mongodb+srv://${process.env.Mongo_User}:6GrevlK6skvsOlpq@cluster0.p6aw3.mongodb.net/<dbname>?retryWrites=true&w=majority`)
    .then(() => {
        app.listen(8000, () => {
            console.log("up and running on port 8000")
        })
    }).catch(err => {
        console.log(err)
    })
